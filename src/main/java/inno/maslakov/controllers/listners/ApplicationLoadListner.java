package inno.maslakov.controllers.listners;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by sa on 24.02.17.
 */
public class ApplicationLoadListner implements ServletContextListener{
    private Logger logger = Logger.getLogger(ApplicationLoadListner.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.trace("Site started");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.trace("Site destroed");
    }
}
