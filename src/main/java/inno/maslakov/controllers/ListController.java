package inno.maslakov.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by sa on 06.03.17.
 */
@Controller
public class ListController {
    Logger logger = Logger.getLogger(ListController.class);
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showLoginPage() {
        logger.error("listPage");
        return "list";
    }
}
