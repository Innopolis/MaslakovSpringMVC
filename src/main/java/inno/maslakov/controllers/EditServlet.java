package inno.maslakov.controllers;

import inno.maslakov.models.pojo.Student;
import inno.maslakov.services.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by bot on 23.02.17.
 */
public class EditServlet extends HttpServlet {
    private static Logger logger = Logger.getLogger(EditServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String idStudent = req.getParameter("idStudent");
        List<Student> studentsList= StudentService.getAllStudents();
        req.setAttribute("student", idStudent);
        req.getRequestDispatcher("/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }
}
