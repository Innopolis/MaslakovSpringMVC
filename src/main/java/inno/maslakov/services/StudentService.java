package inno.maslakov.services;

import inno.maslakov.models.dao.StudentDao;
import inno.maslakov.models.pojo.Student;

import java.util.List;

/**
 * Created by bot on 23.02.17.
 */
public class StudentService {

    public static List<Student> getAllStudents(){
        return StudentDao.getAllStudents();
    }

    public static Student getStudentById(int id){
        return StudentDao.getStudentById(id);
    }

}
