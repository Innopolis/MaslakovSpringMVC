package inno.maslakov.services;

import inno.maslakov.exceptions.UserDaoException;

/**
 * Created by sa on 02.03.17.
 */
public interface IUserService {
    public boolean authorize(String login, String password) throws UserDaoException;
}
