package inno.maslakov.services;

import inno.maslakov.exceptions.UserDaoException;
import inno.maslakov.models.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bot on 23.02.17.
 */
@Service
public class UserService implements IUserService {
    @Autowired
    private UserDao userDao;

    @Override
    public boolean authorize(String login, String password) throws UserDaoException {
        if(userDao.getUserByLoginAndPassword(login, password) != null){
            return true;
        }
        return false;
    }

    public static boolean registration(String login, String password){
        return UserDao.registrationUser(login, password);
    }
}
