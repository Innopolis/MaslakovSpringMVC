package inno.maslakov.models.pojo;

/**
 * Created by bot on 23.02.17.
 */
public class Student {
    public Student(int id, String name, String burthday, String sex, int idGroup) {
        this.id = id;
        this.name = name;
        this.burthday = burthday;
        this.sex = sex;
        this.idGroup = idGroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBurthday() {
        return burthday;
    }

    public void setBurthday(String burthday) {
        this.burthday = burthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    private int id;
    private String name;
    private String burthday;
    private String sex;
    private int idGroup;
}
