package inno.maslakov.models.dao;

import inno.maslakov.models.connector.Connector;
import inno.maslakov.models.pojo.Student;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bot on 23.02.17.
 */
public class StudentDao {

    private static Logger logger = Logger.getLogger(UserDao.class);

    private static String SQL_ALL_STUDENTS = "SELECT * FROM students";
    private static String SQL_ID_STUDENT = "SELECT * FROM students WHERE id = ";

    public static List<Student> getAllStudents(){
        List<Student> studentsList = new ArrayList<>();
        try(Connection connection = Connector.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_ALL_STUDENTS);

            while(resultSet.next()) {
                logger.debug(resultSet.getString("name"));

                Student student = new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("birthday"),
                        resultSet.getString("sex"),
                        resultSet.getInt("group_id")
                );
                studentsList.add(student);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return studentsList;
    }


    public static Student getStudentById(int id) {
        try(Connection connection = Connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_ID_STUDENT + id);
            while(resultSet.next()) {
                Student student = new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("birthday"),
                        resultSet.getString("sex"),
                        resultSet.getInt("group_id")
                );
                return student;
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }
}
