<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>List</h1>
<table border="1">
    <c:forEach items="${studentList}" var="student">
        <form action="/students/list" method="post">
            <tr>
                <td>
                    <input type='text' readonly name='idStudent'
                           value=<c:out value="${student.id}"></c:out>>
                </td>
                <td><c:out value="${student.name}"></c:out></td>
                <td><c:out value="${student.sex}"></c:out></td>
                <td><c:out value="${student.burthday}"></c:out></td>
                <td><c:out value="${student.idGroup}"></c:out></td>
                <td><input type="submit" name="operation"
                           value="Edit" formmethod="post">
                </td>
            </tr>
        </form>

        <%--<c:out value="${userItem.name}"></c:out>--%>
        <%--<c:out value="${userItem.type}"></c:out>--%>
    </c:forEach>
</table>
</body>
</html>
